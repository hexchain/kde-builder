#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2010, 2011, 2012, 2013, 2017, 2018, 2022 Michael Pyne <mpyne@kde.org>
# SPDX-FileCopyrightText: 2023 - 2024 Andrew Shark <ashark@linuxcomp.ru>
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

import argparse
import logging.config
import os
import sys
import textwrap
import traceback
from ksblib.Debug import kbLogger

RealBinDir = os.path.dirname(os.path.realpath(__file__))


# <editor-fold desc="Check missing python modules">
def findMissingModules() -> list:
    requiredModules = [
        "yaml",
        "promise",
        "setproctitle",
    ]
    missingModules = []

    def validateMod(mod_name):
        import importlib

        try:
            importlib.import_module(mod_name)
            return True
        except ImportError:
            return False

    for neededModule in requiredModules:
        if validateMod(neededModule):
            continue
        missingModules.append(neededModule)

    return missingModules


# Ensure that required Python modules are available so that the user isn't surprised later with an exception
missingModuleDescriptions = findMissingModules()
if missingModuleDescriptions:
    print(textwrap.dedent("""\
        Some mandatory Python modules are missing, and kde-builder cannot operate without them. Please ensure these modules are installed:"""))
    for missingModuleDesc in missingModuleDescriptions:
        print("\t" + missingModuleDesc)

    print("\nKDE Builder can do this for you on many distros. Consult the installation process in project documentation at https://kde-builder.kde.org\n"
          "Ensure you have successfully executed this script:\n"
          f"\t{RealBinDir}/scripts/initial_setup.sh")
    exit(1)
# </editor-fold>

# ---

import setproctitle  # noqa: E402
setproctitle.setproctitle("kde-builder main: " + " ".join(sys.argv))

parser = argparse.ArgumentParser(prog="ProgramName",
                                 description="What the program does",
                                 epilog="Text at the bottom of help",
                                 add_help=False)
parser.add_argument("--initial-setup", action="store_true")
parser.add_argument("--install-distro-packages", action="store_true")
parser.add_argument("--generate-config", action="store_true")
parser.add_argument("--debug", action="store_true")

args, unknown = parser.parse_known_args()

# ---

import yaml  # noqa: E402

# <editor-fold desc="Applying loggers configuration">
with open(f"{RealBinDir}/data/kde-builder-logging.yaml", "r") as f:
    config = yaml.safe_load(f.read())

override_config = {}
if os.path.exists(os.getcwd() + "/kde-builder-logging.yaml"):
    with open(os.getcwd() + "/kde-builder-logging.yaml", "r") as f:
        override_config = yaml.safe_load(f.read())
elif os.path.exists(os.path.expanduser("~/.config/kde-builder-logging.yaml")):
    with open(os.path.expanduser("~/.config/kde-builder-logging.yaml"), "r") as f:
        override_config = yaml.safe_load(f.read())


# Merges dict "b" into dict "a", overwriting existing entries. Mutates dict "a". Also, returns "a".
def merge_dicts(a: dict, b: dict, path=None):
    if path is None:
        path = []

    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge_dicts(a[key], b[key], path + [str(key)])
            elif a[key] != b[key]:
                a[key] = b[key]
        else:
            a[key] = b[key]
    return a


config = merge_dicts(config, override_config)
logging.config.dictConfig(config)  # this will create standard Logger loggers and their handlers with their formatters will be set up

# Now will create our kbLogger loggers, set up their levels and handlers
for logger_name, logger_config in config.get("loggers", {}).items():
    level = logger_config.get("level", logging.NOTSET)  # get level from the config
    handlers = logging.getLogger(logger_name).handlers  # get the object handlers that were set for standard logger (their formatters are already set)

    kblogger = kbLogger.getLogger(logger_name, level)  # instantiating our kblogger, and its level becomes set
    for handler in handlers:
        kblogger.addHandler(handler)

if args.debug:
    # setting all loggers to logging.DEBUG level
    for logger_name, _ in config.get("loggers", {}).items():
        kblogger = kbLogger.getLogger(logger_name)
        kblogger.setLevel(logging.DEBUG)
    sys.argv.remove("--debug")  # remove it, so we can pass sys.argv to Application
# </editor-fold>

# ---

setup_steps = []
if args.initial_setup:
    setup_steps = ["install-distro-packages", "generate-config"]
else:
    if args.install_distro_packages:
        setup_steps.append("install-distro-packages")
    if args.generate_config:
        setup_steps.append("generate-config")

if setup_steps:
    from ksblib.FirstRun import FirstRun
    from ksblib.Debug import Debug

    d = Debug()
    d.setColorfulOutput(True)
    fr = FirstRun()
    exit(fr.setupUserSystem(RealBinDir, setup_steps))

from ksblib.Version import Version  # noqa: E402
from ksblib.Application import Application  # noqa: E402
from ksblib.BuildException import BuildException  # noqa: E402

Version.setBasePath(RealBinDir)

# Script starts.

# pl2py: Here there was a check if caller is "test" and if yes, it printed script version and returned 1. It was never used.

app = None
try:
    app = Application(sys.argv[1:])  # the 0 element is script name
    result = app.runAllModulePhases()
    app.finish(result)
except BuildException as err:
    print(textwrap.dedent(f"""
    kde-builder encountered an exceptional error condition:
     ========
        {err}
     ========
    Can't continue, so stopping now.
    
    """))

    if err.exception_type == "Internal":
        print("Please submit a bug against kde-builder on https://invent.kde.org/sdk/kde-builder/-/issues")

    traceback.print_exc()
    if app:
        app.finish(99)  # noreturn
    exit(99)  # if app couldn't be created

except Exception as err:
    # We encountered some other kind of error that didn't raise a ksb::BuildException
    print(textwrap.dedent(f"""\
    Encountered an error in the execution of the script.
    --> {err}
    Please submit a bug against kde-builder on https://invent.kde.org/sdk/kde-builder/-/issues
    """))

    traceback.print_exc()
    if app:
        app.finish(99)  # noreturn
    exit(99)  # if app couldn't be created
